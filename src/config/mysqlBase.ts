import { IP } from './constant';

type NodeEnv = {
  /**
   *数据库名
   * @type {string}
   */
  mysqlName: string;
  /**
   *数据库用户名
   * @type {string}
   */
  mysqlUserName: string;
  /**
   *数据库用户密码
   * @type {string}
   */
  mysqlPassword: string;
  /**
   *mysql部署的机器IP
   * @type {string}
   */
  mysqlIp: string;
};

/** 开发环境数据库配置 */
const development: NodeEnv = {
  mysqlName: 'sys_oa_db',
  mysqlUserName: 'root',
  mysqlPassword: 'a123456',
  mysqlIp: '127.0.0.1'
};

/** 生产环境数据库配置 */
const production: NodeEnv = {
  mysqlName: 'sys_oa_db',
  mysqlUserName: 'mysql',
  mysqlPassword: 'mysqldev',
  mysqlIp: IP
};

export default {
  development,
  production
}[process.env.NODE_ENV || 'development'] ?? development;
