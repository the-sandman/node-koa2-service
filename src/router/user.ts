import Router from 'koa-router';
import UserController from '../controller/user';

const router = new Router({ prefix: '/user' });

/**
 * @swagger
 * /user/addUser: # 接口地址
 *   post: # 请求体
 *     description: 新增用户 # 接口信息
 *     tags: [用户模块] # 模块名称
 *     produces:
 *       - application/x-www-form-urlencoded # 响应内容类型
 *     parameters: # 请求参数
 *       - name: name
 *         description: 用户名
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: 用户密码
 *         in: formData # 参数的位置
 *         required: true
 *         type: string
 *     responses:
 *       '200':
 *         description: Ok
 *         schema: # 返回体说明
 *           type: 'object'
 *           properties:
 *             code:
 *               type: 'number'
 *             message:
 *               type: 'string'
 *               description: 消息提示
 *             data:
 *               type: 'object'
 *               description: 返回数据
 */
router.post('/addUser', UserController.addUser);

router.delete('/deleteUser', UserController.deleteUser);

router.get('/getUserInfo', UserController.getUserInfo);

router.post('/allocationRole', UserController.allocationRole);

export default router;
