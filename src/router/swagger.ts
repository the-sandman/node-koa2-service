import Router from 'koa-router';
import type { Context } from 'koa';
import swaggerJSDoc from '../config/swaggerCfg';

const router = new Router();

router.get('/swagger.json', (ctx: Context) => {
  ctx.set('Content-Type', 'application/json');
  ctx.body = swaggerJSDoc;
});

export default router;
