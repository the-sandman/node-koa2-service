import Router from 'koa-router';
import AuthController from '../controller/auth';

const router = new Router({ prefix: '/auth' });

/**
 * @openapi
 * /:
 *   get:
 *     description: 获取RSA公钥
 *     responses:
 *       200:
 *         description: Returns a mysterious string.
 */
router.get('/publicKey', AuthController.fetchGeneratekey);

/**
 * @openapi
 * /:
 *   get:
 *     description: RSA加密（测试）
 *     responses:
 *       200:
 *         description: Returns a mysterious string.
 */
router.post('/encrypt', AuthController.encrypt);

/**
 * @openapi
 * /:
 *   get:
 *     description: 登录
 *     responses:
 *       data:
 */
router.post('/login', AuthController.login);

export default router;
