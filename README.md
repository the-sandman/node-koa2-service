<div align="center">
	<h1>XXX管理系统</h1>
</div>

## 简介

XXX 管理系统，采用 NodeJS 为语言开发，使用 MySQL 数据库和 Sequelize ORM 框架，服务端框架采用轻量框架 Koa2，同时引入 TypeScript 并有完整的类型文件，可以用于学习参考。

## 项目结构

```
|-- src						// 源代码
    |-- config						// 配置目录
    |-- controller					// 控制器
    |-- database					// SQL文件
    |-- middleware					// 中间件
    |-- model						// 数据库模型
    |-- router						// 路由接口
    |-- service						// 服务层
    |-- typings						// 全局类型文件
    |-- utils						// 工具类
    |-- app.ts						// 入口文件
|-- .cz-config.js				// Git提交规范
|-- .editorconfig				// 编辑器的编码格式规范
|-- .eslintignore				// Eslint忽略检查文件
|-- .eslintrc.js				// Eslint配置
|-- .gitignore					// git忽略文件
|-- .prettierignore				// 代码美化忽略文件
|-- .prettierrc					// 代码美化配置
|-- LICENSE					// 软件许可证
|-- package.json				// 版本管理
|-- pnpm-lock.yaml				// 现代包管理工具
|-- README.md					// 项目文档
├-- tsconfig.js                         	// TypeScript 控制
```

## 需要预先下载模块

- 全局安装 nodemon，ts-node，pnpm。
- Node 版本 >= 10.0.0
- MySQL 数据库。

## 使用说明

1. src/config/mysqlBase.ts 修改文件中数据库配置
2. pnpm install 拉取项目依赖
3. 执行 src/database 中 sql 文件创建表
4. pnpm run dev 运行项目

## 参与贡献

- Fork 本仓库
- 新建 Feat_xxx 分支
- 提交代码
- 新建 Pull Reques
